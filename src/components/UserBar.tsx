import React, { useMemo, MouseEventHandler, useState, useRef } from 'react';
import classNames from 'classnames';

import { humanizeTimestamp } from '../utils';
import { ElementOrComponentOrLiteralType, PropsWithElementAttributes } from '../utils';
import { useTranslationContext } from '../context';
import { Avatar } from './Avatar';
import { useOnClickOutside } from '../hooks/useOnClickOutside';
import Tooltip from './Tooltip';
import { tiers, tagImages } from '../utils/lol';

export type UserBarProps = PropsWithElementAttributes<{
  username: string;
  activePost?: boolean;
  AfterUsername?: React.ReactNode;
  avatar?: string;
  BackButton?: React.ReactNode;
  elo?: string;
  follow?: boolean;
  icon?: string;
  isOwner?: boolean;
  onClickUser?: MouseEventHandler;
  onDeleteResource?: () => void;
  onReportResource?: () => void;
  Right?: ElementOrComponentOrLiteralType;
  subtitle?: string;
  tags?: string[];
  time?: string; // text that should be displayed as the time
  timestamp?: string | number | Date; // a timestamp that should be humanized
}>;

export const UserBar = ({
  avatar,
  activePost,
  elo,
  tags,
  time,
  timestamp,
  BackButton,
  username,
  className,
  style,
  isOwner,
  onClickUser,
  onDeleteResource,
  onReportResource,
}: UserBarProps) => {
  const { tDateTimeParser } = useTranslationContext();

  const [showActionBar, setShowActionBar] = useState(false);
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const dropdownBoxReference = useRef<HTMLDivElement | null>(null);

  const closeActionBar = () => {
    setShowActionBar(false);
    setShowConfirmDelete(false);
  };

  useOnClickOutside(dropdownBoxReference, closeActionBar, showActionBar);

  const handleClickActionTrigger = () => {
    setShowActionBar(true);
  };

  const handleDelete = () => {
    if (showConfirmDelete) {
      if (onDeleteResource) {
        onDeleteResource();
        closeActionBar();
      }
    } else {
      setShowConfirmDelete(true);
    }
  };

  const handleReport = () => {
    if (onReportResource) {
      onReportResource();
      closeActionBar();
    }
  };

  const [humanReadableTimestamp, parsedTimestamp] = useMemo(
    () => [
      !time && timestamp ? humanizeTimestamp(timestamp, tDateTimeParser) : time,
      timestamp ? tDateTimeParser(timestamp).toJSON() : undefined,
    ],
    [timestamp, tDateTimeParser],
  );

  return (
    <>
      {activePost ? (
        <div className="raf-user-bar-active">
          {BackButton}
          {avatar && <Avatar onClick={onClickUser} size={39} circle image={avatar} />}
          <p className="raf-user-bar__username" onClick={onClickUser}>
            {username}
          </p>
          {!!elo && tiers[elo] && (
            <Tooltip description={tiers[elo].name}>
              <img src={tiers[elo].img} className="raf-user-bar__elo" />
            </Tooltip>
          )}
          {!!tags && tags.length > 0 && (
            <div className="raf-user-bar__tags-container">
              {tags.map((e) => (
                <>{tagImages[e] && <img src={tagImages[e]} className="raf-user-bar__tag" />}</>
              ))}
            </div>
          )}
          <time dateTime={parsedTimestamp} title={parsedTimestamp}>
            {humanReadableTimestamp}
          </time>
        </div>
      ) : (
        <div className={classNames('raf-user-bar', className)} style={style}>
          <div className="raf-user-bar__details">
            <div className="raf-user-main-data-container">
              <p data-testid="user-bar-username" className="raf-user-bar__username" onClick={onClickUser}>
                {username}
              </p>
              {elo && tiers[elo] && (
                <Tooltip description={tiers[elo].name}>
                  <img src={tiers[elo].img} className="raf-user-bar__elo" />
                </Tooltip>
              )}
              {tags?.length && (
                <div className="raf-user-bar__tags-container">
                  {tags.map((e) => (
                    <>{tagImages[e] && <img src={tagImages[e]} className="raf-user-bar__tag" />}</>
                  ))}
                </div>
              )}
              <p className="raf-user-bar__extra">
                <time dateTime={parsedTimestamp} title={parsedTimestamp}>
                  {humanReadableTimestamp}
                </time>
              </p>
            </div>
            <div className={`raf-action-dropdown show-children-actionbar-${showActionBar}`}>
              <button className="raf-action-dropdown-trigger" onClick={handleClickActionTrigger}>
                ...
              </button>
              <div className={`raf-action-dropdown-content show-actionbar-${showActionBar}`} ref={dropdownBoxReference}>
                <button className="raf-action-dropdown-button" onClick={handleReport}>
                  Reportar
                </button>
                <hr />
                {isOwner && (
                  <>
                    <button
                      className={`raf-action-dropdown-button show-confirm-${showConfirmDelete}`}
                      onClick={handleDelete}
                    >
                      {showConfirmDelete ? 'Confirmar' : 'Deletar'}
                    </button>
                    <hr />
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
