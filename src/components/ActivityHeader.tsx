import React from 'react';

import { DefaultAT, DefaultUT } from '../context/StreamApp';
import { useTranslationContext } from '../context/TranslationContext';
import { userOrDefault, humanizeTimestamp, useOnClickUser } from '../utils';
import { ActivityProps } from './Activity';
import { UserBar } from './UserBar';

export type ActivityHeaderProps<UT extends DefaultUT = DefaultUT, AT extends DefaultAT = DefaultAT> = Pick<
  ActivityProps<UT, AT>,
  | 'activity'
  | 'BackButton'
  | 'HeaderRight'
  | 'icon'
  | 'onClickUser'
  | 'className'
  | 'style'
  | 'activePost'
  | 'currentUserId'
  | 'onDeleteResource'
  | 'onReportResource'
>;

export const ActivityHeader = <UT extends DefaultUT = DefaultUT, AT extends DefaultAT = DefaultAT>({
  activity,
  activePost,
  BackButton,
  HeaderRight,
  icon,
  currentUserId,
  onClickUser,
  onDeleteResource,
  onReportResource,
  style,
  className,
}: ActivityHeaderProps<UT, AT>) => {
  const { tDateTimeParser } = useTranslationContext();

  const actor = userOrDefault<UT>(activity.actor);
  const handleUserClick = useOnClickUser<UT>(onClickUser);

  return (
    <div style={style} className={className}>
      <UserBar
        activePost={activePost}
        username={actor.data.name}
        avatar={actor.data.profileImage}
        elo={actor.data.elo}
        tags={actor.data.tags}
        onClickUser={handleUserClick?.(actor)}
        isOwner={currentUserId === actor.id}
        onReportResource={onReportResource}
        onDeleteResource={onDeleteResource}
        subtitle={HeaderRight ? humanizeTimestamp(activity.time, tDateTimeParser) : undefined}
        timestamp={activity.time}
        icon={icon}
        Right={HeaderRight}
        BackButton={BackButton}
      />
    </div>
  );
};
