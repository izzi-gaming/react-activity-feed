import React, { useRef, useState } from 'react';
import classNames from 'classnames';
import { EnrichedReaction, UR } from 'getstream';
import Tooltip from './Tooltip';
import { Avatar } from './Avatar';
import {
  humanizeTimestamp,
  textRenderer,
  OnClickUserHandler,
  useOnClickUser,
  PropsWithElementAttributes,
} from '../utils';
import { useTranslationContext } from '../context';
import { DefaultUT } from '../context/StreamApp';
import { useOnClickOutside } from '../hooks/useOnClickOutside';
import { tiers, tagImages } from '../utils/lol';

export type CommentItemProps<
  UT extends DefaultUT = DefaultUT,
  RT extends UR = UR,
  CRT extends UR = UR
> = PropsWithElementAttributes<
  {
    comment: EnrichedReaction<RT, CRT, UT>;
    currentUserId?: string;
    onClickUser?: OnClickUserHandler<UT>;
    onDeleteResource?: (id: string) => Promise<boolean>;
    onReportResource?: (id: string) => void;
  } & Partial<Record<'onClickMention' | 'onClickHashtag', (word: string) => void>>
>;

export const CommentItem = <UT extends DefaultUT = DefaultUT, RT extends UR = UR, CRT extends UR = UR>({
  comment: { user, created_at, data, id },
  currentUserId,
  onClickHashtag,
  onClickMention,
  onClickUser,
  onReportResource,
  onDeleteResource,
  className,
  style,
}: CommentItemProps<UT, RT, CRT>) => {
  const { tDateTimeParser } = useTranslationContext();
  const userElo = user?.data?.elo;
  const userTags = user?.data?.tags || [];
  const [isDeleted, setIsDeleted] = useState(false);
  const [showActionBar, setShowActionBar] = useState(false);
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const dropdownBoxReference = useRef<HTMLDivElement | null>(null);

  const closeActionBar = () => {
    setShowActionBar(false);
    setShowConfirmDelete(false);
  };

  useOnClickOutside(dropdownBoxReference, closeActionBar, showActionBar);

  const handleClickActionTrigger = () => {
    setShowActionBar(true);
  };

  const handleDelete = async (resourceId: string) => {
    if (showConfirmDelete) {
      if (onDeleteResource) {
        closeActionBar();
        const successfullyDeleted = await onDeleteResource(resourceId);
        if (successfullyDeleted) {
          setIsDeleted(true);
        }
      }
    } else {
      setShowConfirmDelete(true);
    }
  };

  const handleReport = (resourceId: string) => {
    if (onReportResource) {
      onReportResource(resourceId);
      closeActionBar();
    }
  };

  const handleUserClick = useOnClickUser<UT, SVGSVGElement | HTMLSpanElement>(onClickUser);

  if (isDeleted) {
    return null;
  }

  return (
    <div className="raf-activity-comment">
      <div className="raf-activity-comment__left_corner">
        {user?.data.profileImage && (
          <Avatar onClick={handleUserClick?.(user)} image={user.data.profileImage} circle size={39} />
        )}
      </div>
      <div className="raf-activity-comment__right_corner">
        <div className={classNames('raf-user-bar', className)} style={style}>
          <div className="raf-user-main-data-container">
            <div className="raf-user-bar__details">
              <p data-testid="user-bar-username" className="raf-user-bar__username">
                {user?.data.name}
              </p>
            </div>
            {userElo && tiers[userElo] && (
              <Tooltip description={tiers[userElo].name}>
                <img src={tiers[userElo].img} className="raf-user-bar__elo" />
              </Tooltip>
            )}
            <div className="raf-user-bar__tags-container">
              {userTags.map((tag) => (
                <>{tagImages[tag] && <img src={tagImages[tag]} className="raf-user-bar__tag" />}</>
              ))}
            </div>
            <p className="raf-user-bar-comment__extra">
              <time dateTime={created_at} title={created_at}>
                {humanizeTimestamp(created_at, tDateTimeParser)}
              </time>
            </p>
          </div>
          <div className={`raf-action-dropdown show-children-actionbar-${showActionBar}`}>
            <button className="raf-action-dropdown-trigger" onClick={handleClickActionTrigger}>
              ...
            </button>
            <div className={`raf-action-dropdown-content show-actionbar-${showActionBar}`} ref={dropdownBoxReference}>
              <button className="raf-action-dropdown-button" onClick={() => handleReport(id)}>
                Reportar
              </button>
              <hr />
              {user?.id === currentUserId && (
                <>
                  <button
                    className={`raf-action-dropdown-button show-confirm-${showConfirmDelete}`}
                    onClick={() => handleDelete(id)}
                  >
                    {showConfirmDelete ? 'Confirmar' : 'Deletar'}
                  </button>
                  <hr />
                </>
              )}
            </div>
          </div>
        </div>
        <div>
          <p>{textRenderer(data.text as string, 'raf-activity-comment', onClickMention, onClickHashtag)}</p>
        </div>
      </div>
    </div>
  );
};
