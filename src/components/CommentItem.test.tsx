import React from 'react';
import renderer from 'react-test-renderer';

import moment from 'moment';
import { CommentItem, CommentItemProps } from './CommentItem';

import { TranslationProvider } from '../context/TranslationContext';

// @ts-expect-error
Date.now = jest.fn(() => new Date('2021-04-15T11:34:36.104Z'));

const translationProviderData = { t: (v: string) => v, tDateTimeParser: moment };

const testUserData = {
  data: {
    name: 'Rosemary',
    subtitle: 'likes playing fresbee in the park',
    profileImage: 'https://randomuser.me/api/portraits/women/20.jpg',
  },
  created_at: '',
  updated_at: '',
  id: '',
};

const testData: CommentItemProps['comment'] = {
  user: undefined,
  created_at: '2021-04-13T07:40:37.975Z',
  data: {
    text: 'Snowboarding is awesome! #snowboarding @mark',
  },
  kind: 'comment',
  id: '',
  activity_id: '',
  parent: '',
  updated_at: '',
  user_id: '',
  children_counts: {},
  latest_children: {},
};

describe('CommentItem', () => {
  it('renders with default props', () => {
    const tree = renderer
      .create(
        <TranslationProvider value={translationProviderData}>
          <CommentItem comment={testData} />
        </TranslationProvider>,
      )
      .toJSON();
    expect(tree).toMatchInlineSnapshot(`
      <div
        className="raf-activity-comment"
      >
        <div
          className="raf-activity-comment__left_corner"
        />
        <div
          className="raf-activity-comment__right_corner"
        >
          <div
            className="raf-user-bar"
          >
            <div
              className="raf-user-main-data-container"
            >
              <div
                className="raf-user-bar__details"
              >
                <p
                  className="raf-user-bar__username"
                  data-testid="user-bar-username"
                />
              </div>
              <div
                className="raf-user-bar__tags-container"
              />
              <p
                className="raf-user-bar-comment__extra"
              >
                <time
                  dateTime="2021-04-13T07:40:37.975Z"
                  title="2021-04-13T07:40:37.975Z"
                >
                  2 days ago
                </time>
              </p>
            </div>
            <div
              className="raf-action-dropdown show-children-actionbar-false"
            >
              <button
                className="raf-action-dropdown-trigger"
                onClick={[Function]}
              >
                ...
              </button>
              <div
                className="raf-action-dropdown-content show-actionbar-false"
              >
                <button
                  className="raf-action-dropdown-button"
                  onClick={[Function]}
                >
                  Reportar
                </button>
                <hr />
                <button
                  className="raf-action-dropdown-button show-confirm-false"
                  onClick={[Function]}
                >
                  Deletar
                </button>
                <hr />
              </div>
            </div>
          </div>
          <div>
            <p>
              Snowboarding
               
              is
               
              awesome!
               
              #snowboarding
               
              @mark
            </p>
          </div>
        </div>
      </div>
    `);
  });

  it('renders with user specified', () => {
    const tree = renderer
      .create(
        <TranslationProvider value={translationProviderData}>
          <CommentItem comment={{ ...testData, user: testUserData }} />
        </TranslationProvider>,
      )
      .toJSON();
    expect(tree).toMatchInlineSnapshot(`
      <div
        className="raf-activity-comment"
      >
        <div
          className="raf-activity-comment__left_corner"
        >
          <img
            alt=""
            className="raf-avatar raf-avatar--circle"
            src="https://randomuser.me/api/portraits/women/20.jpg"
            style={
              Object {
                "height": "39px",
                "width": "39px",
              }
            }
          />
        </div>
        <div
          className="raf-activity-comment__right_corner"
        >
          <div
            className="raf-user-bar"
          >
            <div
              className="raf-user-main-data-container"
            >
              <div
                className="raf-user-bar__details"
              >
                <p
                  className="raf-user-bar__username"
                  data-testid="user-bar-username"
                >
                  Rosemary
                </p>
              </div>
              <div
                className="raf-user-bar__tags-container"
              />
              <p
                className="raf-user-bar-comment__extra"
              >
                <time
                  dateTime="2021-04-13T07:40:37.975Z"
                  title="2021-04-13T07:40:37.975Z"
                >
                  2 days ago
                </time>
              </p>
            </div>
            <div
              className="raf-action-dropdown show-children-actionbar-false"
            >
              <button
                className="raf-action-dropdown-trigger"
                onClick={[Function]}
              >
                ...
              </button>
              <div
                className="raf-action-dropdown-content show-actionbar-false"
              >
                <button
                  className="raf-action-dropdown-button"
                  onClick={[Function]}
                >
                  Reportar
                </button>
                <hr />
              </div>
            </div>
          </div>
          <div>
            <p>
              Snowboarding
               
              is
               
              awesome!
               
              #snowboarding
               
              @mark
            </p>
          </div>
        </div>
      </div>
    `);
  });

  it('renders with hashtags and mentions', () => {
    const tree = renderer
      .create(
        <TranslationProvider value={translationProviderData}>
          <CommentItem onClickHashtag={console.log} onClickMention={console.log} comment={testData} />
        </TranslationProvider>,
      )
      .toJSON();
    expect(tree).toMatchInlineSnapshot(`
      <div
        className="raf-activity-comment"
      >
        <div
          className="raf-activity-comment__left_corner"
        />
        <div
          className="raf-activity-comment__right_corner"
        >
          <div
            className="raf-user-bar"
          >
            <div
              className="raf-user-main-data-container"
            >
              <div
                className="raf-user-bar__details"
              >
                <p
                  className="raf-user-bar__username"
                  data-testid="user-bar-username"
                />
              </div>
              <div
                className="raf-user-bar__tags-container"
              />
              <p
                className="raf-user-bar-comment__extra"
              >
                <time
                  dateTime="2021-04-13T07:40:37.975Z"
                  title="2021-04-13T07:40:37.975Z"
                >
                  2 days ago
                </time>
              </p>
            </div>
            <div
              className="raf-action-dropdown show-children-actionbar-false"
            >
              <button
                className="raf-action-dropdown-trigger"
                onClick={[Function]}
              >
                ...
              </button>
              <div
                className="raf-action-dropdown-content show-actionbar-false"
              >
                <button
                  className="raf-action-dropdown-button"
                  onClick={[Function]}
                >
                  Reportar
                </button>
                <hr />
                <button
                  className="raf-action-dropdown-button show-confirm-false"
                  onClick={[Function]}
                >
                  Deletar
                </button>
                <hr />
              </div>
            </div>
          </div>
          <div>
            <p>
              Snowboarding
               
              is
               
              awesome!
               
              <a
                className="raf-activity-comment__hashtag"
                onClick={[Function]}
              >
                #snowboarding
              </a>
               
              <a
                className="raf-activity-comment__mention"
                onClick={[Function]}
              >
                @mark
              </a>
            </p>
          </div>
        </div>
      </div>
    `);
  });
});
