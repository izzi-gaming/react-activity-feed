import React, { InputHTMLAttributes, ReactNode } from 'react';

type TooltipProps = {
  children: ReactNode;
  description: ReactNode | string;
} & InputHTMLAttributes<HTMLInputElement>;

const Tooltip = ({ children, description }: TooltipProps) => (
  <div className="raf-tooltip-container">
    <div className="raf-tooltip-trigger">{children}</div>
    <div className="raf-tooltip-description">{description}</div>
  </div>
);

export default Tooltip;
