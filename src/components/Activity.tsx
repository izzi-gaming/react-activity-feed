import React from 'react';
import classNames from 'classnames';
import { EnrichedActivity, UR } from 'getstream';

import { ActivityContent, ActivityContent as DefaultActivityContent, ActivityContentProps } from './ActivityContent';
import { ActivityHeader, ActivityHeader as DefaultActivityHeader, ActivityHeaderProps } from './ActivityHeader';
import { Card as DefaultCard, CardProps } from './Card';
import { ActivityFooterProps } from './ActivityFooter';
import { userOrDefault, useOnClickUser } from '../utils';

import {
  smartRender,
  ElementOrComponentOrLiteralType,
  UserOrDefaultReturnType,
  PropsWithElementAttributes,
} from '../utils';
import { DefaultAT, DefaultUT } from '../context/StreamApp';
import { Avatar } from './Avatar';

type WordClickHandler = (word: string) => void;

export type ActivityProps<
  UT extends DefaultUT = DefaultUT,
  AT extends DefaultAT = DefaultAT,
  CT extends UR = UR,
  RT extends UR = UR,
  CRT extends UR = UR
> = PropsWithElementAttributes<{
  /** The activity received for stream for which to show the like button. This is
   * used to initialize the toggle state and the counter. */
  activity: EnrichedActivity<UT, AT, CT, RT, CRT>;
  /** Used to check if post detail is active */
  activePost?: boolean;
  /** Back button component in post detail */
  BackButton?: ElementOrComponentOrLiteralType;
  /** Card component to display.
   * #Card (Component)#
   */
  Card?: ElementOrComponentOrLiteralType<CardProps>;
  /** Content component to display.
   * #ActivityContent (Component)#
   */
  Content?: ElementOrComponentOrLiteralType<ActivityContentProps<UT, AT, CT, RT, CRT>>;
  /** The feed group part of the feed that the activity should be reposted to
   * when pressing the RepostButton, e.g. `user` when posting to your own profile
   * defaults to 'user' feed */
  currentUserId?: string;
  feedGroup?: string;
  Footer?: ElementOrComponentOrLiteralType<ActivityFooterProps<UT, AT, CT, RT, CRT>>;
  /** Header component to display.
   * #ActivityHeader (Component)#
   */
  Header?: ElementOrComponentOrLiteralType<ActivityHeaderProps<UT, AT>>;
  HeaderRight?: ElementOrComponentOrLiteralType;
  icon?: string;
  /** Handler for any routing you may do on clicks on Hashtags */
  onClickHashtag?: WordClickHandler;
  /** Handler for any routing you may do on clicks on Mentions */
  onClickMention?: WordClickHandler;
  onClickUser?: (user: UserOrDefaultReturnType<UT>) => void;
  onDeleteResource?: () => void;
  onReportResource?: () => void;
  /** UI component to render original activity within a repost
   * #Repost (Component)#
   */
  Repost?: ElementOrComponentOrLiteralType<ActivityProps<UT, AT, CT, RT, CRT>>;
  /** The user_id part of the feed that the activity should be reposted to when
   * pressing the RepostButton */
  userId?: string;
}>;

const DefaultRepost = <
  UT extends DefaultUT = DefaultUT,
  AT extends DefaultAT = DefaultAT,
  CT extends UR = UR,
  RT extends UR = UR,
  CRT extends UR = UR
>({
  Header = DefaultActivityHeader,
  HeaderRight,
  Content = DefaultActivityContent,
  activity,
  icon,
  onClickHashtag,
  onClickMention,
  onClickUser,
}: ActivityProps<UT, AT, CT, RT, CRT>) => (
  <div className="raf-card raf-activity raf-activity-repost">
    {smartRender<ActivityHeaderProps<UT, AT>>(Header, { HeaderRight, icon, activity, onClickUser })}
    {smartRender<ActivityContentProps<UT, AT, CT, RT, CRT>>(Content, { onClickMention, onClickHashtag, activity })}
  </div>
);

export const Activity = <
  UT extends DefaultUT = DefaultUT,
  AT extends DefaultAT = DefaultAT,
  CT extends UR = UR,
  RT extends UR = UR,
  CRT extends UR = UR
>({
  Header = DefaultActivityHeader,
  HeaderRight,
  Content = DefaultActivityContent,
  Footer,
  Card = DefaultCard,
  activity,
  BackButton,
  icon,
  onClickHashtag,
  onClickMention,
  onClickUser,
  Repost = DefaultRepost,
  activePost,
  userId,
  feedGroup,
  className,
  style,
  currentUserId,
  onDeleteResource,
  onReportResource,
}: ActivityProps<UT, AT, CT, RT, CRT>) => {
  const actor = userOrDefault<UT>(activity.actor);
  const handleUserClick = useOnClickUser<UT>(onClickUser);

  const avatar = actor.data.profileImage;
  return (
    <div className={classNames(`raf-activity raf-activity-active-${activePost}`, className)} style={style}>
      {activePost ? (
        <div className="raf-activity-active">
          <ActivityHeader
            activePost
            BackButton={BackButton}
            activity={activity}
            icon={icon}
            currentUserId={currentUserId}
            onDeleteResource={onDeleteResource}
            onReportResource={onReportResource}
            onClickUser={onClickUser}
          />
          <ActivityContent
            activity={activity}
            Content={Content}
            Card={Card}
            feedGroup={feedGroup}
            Footer={Footer}
            Header={Header}
            HeaderRight={HeaderRight}
            icon={icon}
            onClickHashtag={onClickHashtag}
            onClickMention={onClickMention}
            onClickUser={onClickUser}
            Repost={Repost}
            userId={userId}
          />
          {smartRender<ActivityFooterProps<UT, AT, CT, RT, CRT>>(Footer, { activity, feedGroup, userId })}
        </div>
      ) : (
        <>
          <div className="raf-activity__left_corner">
            {avatar && <Avatar onClick={handleUserClick?.(actor)} size={39} circle image={avatar} />}
          </div>
          <div className="raf-activity__right_corner">
            {smartRender<ActivityHeaderProps<UT, AT>>(Header, {
              HeaderRight,
              icon,
              currentUserId,
              activity,
              onClickUser,
              onDeleteResource,
              onReportResource,
            })}
            {smartRender<ActivityContentProps<UT, AT, CT, RT, CRT>>(Content, {
              activity,
              Content,
              Card,
              feedGroup,
              Footer,
              Header,
              HeaderRight,
              icon,
              onClickHashtag,
              onClickMention,
              onClickUser,
              Repost,
              userId,
            })}
            {smartRender<ActivityFooterProps<UT, AT, CT, RT, CRT>>(Footer, { activity, feedGroup, userId })}
          </div>
        </>
      )}
    </div>
  );
};
