export const tiers: { [key: string]: { img: string; name: string } } = {
  UNRANKED: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Unranked.png',
    name: 'Unranked',
  },
  BRONZE: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Bronze.png',
    name: 'Bronze',
  },
  SILVER: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Silver.png',
    name: 'Prata',
  },
  GOLD: { img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Gold.png', name: 'Ouro' },
  DIAMOND: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Diamond.png',
    name: 'Diamante',
  },
  IRON: { img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Iron.png', name: 'Ferro' },
  MASTER: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Master.png',
    name: 'Mestre',
  },
  PLATINUM: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Platinum.png',
    name: 'Platina',
  },
  GRANDMASTER: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Grandmaster.png',
    name: 'Grão Mestre',
  },
  CHALLENGER: {
    img: 'https://storage.googleapis.com/izzi-gaming/gaming/lol/images/emblems/Emblem_Challenger.png',
    name: 'Desafiante',
  },
};

export const tagImages: { [key: string]: string } = {
  COACH: 'https://storage.googleapis.com/izzi-gaming/gaming/coach-tag.svg',
};
